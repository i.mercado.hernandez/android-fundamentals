package com.example.proyectocursoandroid.screens.pokedex

import com.example.proyectocursoandroid.models.pokedex.*
import com.example.proyectocursoandroid.repositories.PokedexRepository
import com.example.proyectocursoandroid.services.PokedexService
import junit.framework.Assert.assertFalse
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import retrofit2.Response

class PokedexViewModelTest {

    private lateinit var viewModel: PokedexViewModel
    private lateinit var repository: PokedexRepository
    private lateinit var pokemonName: String

    @Before
    fun setup(){
        repository = PokedexRepository(PokedexService.getInstance())
        viewModel = PokedexViewModel(repository)
        pokemonName = "gengar"
    }

    @Test
    fun getPokemonListResponseTest(){
        val response = repository.getPokemonList().execute()
        assert(response.body() is PokemonSpeciesResponse)

    }

    @Test
    fun getPokemonListNotEmptyTest(){
        val response = repository.getPokemonList().execute()
        assert(response.body()?.results!!.isNotEmpty())
    }

    @Test
    fun getPokemonDetailNameTest(){
        val repository = Mockito.spy(PokedexRepository(PokedexService.getInstance()))
        Mockito.doReturn(Response.success(getPokemonDetailSample())).`when`(repository.getPokemonDetail(pokemonName))
        val response = repository.getPokemonDetail(pokemonName).execute().body()
        assert(response is PokemonModel && response.name.isNotEmpty())
    }

    @Test
    fun getPokemonDetailMovesTest(){
        val response = repository.getPokemonDetail(pokemonName).execute()
        assert(response.body()!!.moves.isNotEmpty())
    }

    @Test
    fun getPokemonDetailAbilityTest(){
        val response = repository.getPokemonDetail(pokemonName).execute()
        val ability = response.body()!!.abilities.find { pokemonAbilities -> !pokemonAbilities.hidden }
        assert(ability != null)
    }

    @Test
    fun getPokemonDetailHiddenAbilityTest(){
        val response = repository.getPokemonDetail(pokemonName).execute()
        val ability = response.body()!!.abilities.find { pokemonAbilities -> pokemonAbilities.hidden }
        assertFalse(ability != null)
    }

    fun getPokemonDetailSample(): PokemonModel {

        val ability = Ability(
            "Resistencia a Fuego"
        )

        val abilities = listOf(
            PokemonAbilities(
                ability,
            false
            )
        )

        val ember = Move("Azcuas")
        val tackle = Move("Placaje")

        val moves = listOf(
            PokemonMoves(ember),
            PokemonMoves(tackle)
        )

        return PokemonModel(
            "charmander",
            4,
            abilities,
            moves
        )
    }
}