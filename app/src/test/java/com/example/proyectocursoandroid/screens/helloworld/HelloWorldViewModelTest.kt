package com.example.proyectocursoandroid.screens.helloworld

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HelloWorldViewModelTest {

    private lateinit var viewModel: HelloWorldViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup(){
        viewModel = HelloWorldViewModel()
    }

    @Test
    fun setCurrentNameWithNoDigitsTest() {
        val currentName = "Iván Ahmed Mercado Hernández"
        viewModel.setCurrentName(currentName)
        val viewModelOutcome = viewModel.currentName.value
        val testResult = viewModelOutcome?.all { !it.isDigit() }!!
        Assert.assertTrue(testResult)
    }
}