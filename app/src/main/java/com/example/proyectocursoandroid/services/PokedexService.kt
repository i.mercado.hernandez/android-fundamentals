package com.example.proyectocursoandroid.services

import com.example.proyectocursoandroid.models.pokedex.PokemonModel
import com.example.proyectocursoandroid.models.pokedex.PokemonSpeciesResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokedexService {

    companion object {
        const val BASE_URL = "http://pokeapi.co/api/v2/"
        var service: PokedexService? = null

        fun getInstance(): PokedexService {

            if(service == null){
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

                service = retrofit.create(PokedexService::class.java)
            }
            return service!!
        }
    }

    @GET("pokemon-species")
    fun getPokemonList(@Query("limit") limit: Int = 151): Call<PokemonSpeciesResponse>

    @GET("pokemon/{pokemon_name}/")
    fun getPokemonDetail(
        @Path("pokemon_name") pokemonName: String
    ): Call<PokemonModel>
}