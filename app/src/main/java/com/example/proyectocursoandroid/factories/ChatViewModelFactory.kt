package com.example.proyectocursoandroid.factories

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.proyectocursoandroid.screens.chat.ChatViewModel
import java.lang.IllegalArgumentException

class ChatViewModelFactory constructor(private val application: Application): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if(modelClass.isAssignableFrom(ChatViewModel::class.java)){
            ChatViewModel(this.application) as T
        } else {
            throw  IllegalArgumentException("ChatViewModel Constructor Not Found")
        }
    }
}