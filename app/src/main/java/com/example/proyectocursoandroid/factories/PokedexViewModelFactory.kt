package com.example.proyectocursoandroid.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.proyectocursoandroid.repositories.PokedexRepository
import com.example.proyectocursoandroid.screens.pokedex.PokedexViewModel
import java.lang.IllegalArgumentException

class PokedexViewModelFactory constructor(private val repository: PokedexRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if(modelClass.isAssignableFrom(PokedexViewModel::class.java)){
            PokedexViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}