package com.example.proyectocursoandroid.screens.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.proyectocursoandroid.databinding.ActivityMainBinding
import com.example.proyectocursoandroid.screens.chat.ChatActivity
import com.example.proyectocursoandroid.screens.helloworld.HelloWorldActivity
import com.example.proyectocursoandroid.screens.mathoperations.MathOperationsActivity
import com.example.proyectocursoandroid.screens.pokedex.PokedexActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setup()


    }

    private fun setup(){
        binding.toHelloWorldButton.setOnClickListener {
            navigateToHelloWorldActivity()
        }
        binding.toMathOperationsButton.setOnClickListener {
            navigateToMathOperationsActivity()
        }
        binding.toPokedexButton.setOnClickListener {
            navigateToPokedexActivity()
        }
        binding.toChatButton.setOnClickListener {
            navigateToChatActivity()
        }
    }

    private fun navigateToHelloWorldActivity() {
        HelloWorldActivity.start(this)
    }

    private fun navigateToMathOperationsActivity(){
        MathOperationsActivity.start(this)
    }

    private fun navigateToPokedexActivity() {
        PokedexActivity.start(this)
    }

    private fun navigateToChatActivity() {
        ChatActivity.start(this)
    }
}