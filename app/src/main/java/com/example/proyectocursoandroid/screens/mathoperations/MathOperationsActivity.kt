package com.example.proyectocursoandroid.screens.mathoperations

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import com.example.proyectocursoandroid.R
import com.example.proyectocursoandroid.databinding.ActivityMathOperationsBinding

class MathOperationsActivity : AppCompatActivity() {

    companion object {

        fun start(context: Context){
            val intent = Intent(context, MathOperationsActivity::class.java)
            context.startActivity(intent)
        }

    }

    private lateinit var binding: ActivityMathOperationsBinding

    private val countDownTimer =  object : CountDownTimer(3000, 1000){
        override fun onTick(millisUntilFinished: Long) {}
        override fun onFinish() {
            binding.operationResultTextView.text = ""
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMathOperationsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setup()
    }

    private fun setup(){
        val stringResult = "El resultado es: "
        binding.sumButton.setOnClickListener {
            if(!areAnyFieldEmpty())
                binding.operationResultTextView.text =
                    stringResult.plus(
                        sum(
                            binding.firstNumberInput.text.toString().toDouble(),
                            binding.secondNumberInput.text.toString().toDouble()
                        )
                    )
            else emptyFieldsWarning()
        }
        binding.subtractButton.setOnClickListener {
            if (!areAnyFieldEmpty())
                binding.operationResultTextView.text =
                    stringResult.plus(
                        subtract(
                            binding.firstNumberInput.text.toString().toDouble(),
                            binding.secondNumberInput.text.toString().toDouble()
                        )
                    )
            else emptyFieldsWarning()
        }
        binding.multiplyButton.setOnClickListener {
            if (!areAnyFieldEmpty())
                binding.operationResultTextView.text =
                    stringResult.plus(
                        multiply(
                            binding.firstNumberInput.text.toString().toDouble(),
                            binding.secondNumberInput.text.toString().toDouble()
                        )
                    )
            else emptyFieldsWarning()
        }
        binding.divideButton.setOnClickListener {
            if (!areAnyFieldEmpty())
                binding.operationResultTextView.text =
                    stringResult.plus(
                        divide(
                            binding.firstNumberInput.text.toString().toDouble(),
                            binding.secondNumberInput.text.toString().toDouble()
                        )
                    )
            else emptyFieldsWarning()
        }
    }

    private fun areAnyFieldEmpty() = binding.firstNumberInput.text.toString().isEmpty() && binding.firstNumberInput.text.toString().isEmpty()

    private fun emptyFieldsWarning(){
        binding.operationResultTextView.text = getString(R.string.imcomplete_fields_text)
        countDownTimer.start()
    }

    private fun sum(first: Double, second: Double): Double{
        return first.plus(second)
    }

    private fun subtract(first: Double, second: Double): Double{
        return first.minus(second)
    }

    private fun multiply(first: Double, second: Double): Double{
        return first.times(second)
    }
    private fun divide(first: Double, second: Double): Double{
        return first.div(second)
    }
}