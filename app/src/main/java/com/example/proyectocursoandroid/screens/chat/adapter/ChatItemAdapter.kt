package com.example.proyectocursoandroid.screens.chat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proyectocursoandroid.databinding.ChatItemLayoutBinding
import com.example.proyectocursoandroid.models.chat.Message

class ChatItemAdapter(
    private val context: Context
    ) : RecyclerView.Adapter<ChatItemAdapter.ViewHolder>() {

        inner class ViewHolder(val binding: ChatItemLayoutBinding): RecyclerView.ViewHolder(binding.root)

    private var messages = mutableListOf<Message>()

    fun setMessages(list: List<Message>){
        this.messages = list.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount() = messages.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ChatItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
           with(messages[position]) {
               binding.messageContainer.text = this.message
           }
        }
    }
}