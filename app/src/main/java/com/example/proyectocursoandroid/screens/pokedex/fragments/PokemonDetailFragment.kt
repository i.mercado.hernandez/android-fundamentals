package com.example.proyectocursoandroid.screens.pokedex.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.proyectocursoandroid.databinding.FragmentPokemonDetailBinding
import com.example.proyectocursoandroid.models.pokedex.PokemonListItem
import com.example.proyectocursoandroid.models.pokedex.PokemonModel
import com.example.proyectocursoandroid.screens.pokedex.PokedexViewModel
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient

class PokemonDetailFragment: Fragment() {

    private lateinit var binding: FragmentPokemonDetailBinding
    private val viewModel: PokedexViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPokemonDetailBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        viewModel.selectedPokemon.observe(viewLifecycleOwner, { pokemon ->
            if(pokemon != null) {
                viewModel.getPokemonDetail(pokemon.name)
            }
        })
        viewModel.pokemonDetail.observe(viewLifecycleOwner, {
            showPokemonInfo(it)
        })
    }

    private fun showPokemonInfo(pokemon: PokemonModel) {
        binding.pokemonName.text = pokemon.name
        val picassoClient = OkHttpClient()
        val picasso = Picasso.Builder(requireContext()).downloader(OkHttp3Downloader(picassoClient)).build()
        picasso.isLoggingEnabled = true
        picasso.load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png").into(binding.pokemonImage)

        pokemon.abilities.forEach { ability ->
            if(!ability.hidden) binding.hiddenAbilityTextView.text = "Hidden Ability: ${ability.ability.name}"
            else binding.abilityTextView.text = "Ability: ${ability.ability.name}"
        }

        binding.firstMoveTextView.text = pokemon.moves[0].move.name
        binding.secondMoveTextView.text = pokemon.moves[1].move.name
        binding.thirdMoveTextView.text = pokemon.moves[2].move.name
        binding.fourthMoveTextView.text = pokemon.moves[3].move.name
    }

}