package com.example.proyectocursoandroid.screens.pokedex

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.proyectocursoandroid.models.pokedex.PokemonListItem
import com.example.proyectocursoandroid.models.pokedex.PokemonModel
import com.example.proyectocursoandroid.models.pokedex.PokemonSpeciesResponse
import com.example.proyectocursoandroid.repositories.PokedexRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokedexViewModel constructor(private val repository: PokedexRepository): ViewModel() {

    val selectedPokemon: MutableLiveData<PokemonListItem?> by lazy {
        MutableLiveData<PokemonListItem?>()
    }

    fun setSelectedPokemon(pokemon: PokemonListItem?){
        selectedPokemon.postValue(pokemon)
    }

    val pokemonList: MutableLiveData<List<PokemonListItem>> by lazy {
        MutableLiveData<List<PokemonListItem>>()
    }

    val errorMessage: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val pokemonDetail: MutableLiveData<PokemonModel> by lazy {
        MutableLiveData<PokemonModel>()
    }

    fun getPokemonList(){
        val response = repository.getPokemonList()
        response.enqueue(object : Callback<PokemonSpeciesResponse> {

            override fun onResponse(
                call: Call<PokemonSpeciesResponse>,
                response: Response<PokemonSpeciesResponse>
            ) {
                pokemonList.postValue(response.body()?.results)
            }

            override fun onFailure(call: Call<PokemonSpeciesResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }

        })
    }

    fun getPokemonDetail(name: String){
        val response = repository.getPokemonDetail(name)
        response.enqueue(object : Callback<PokemonModel>{

            override fun onResponse(call: Call<PokemonModel>, response: Response<PokemonModel>) {
                pokemonDetail.postValue(response.body())
            }

            override fun onFailure(call: Call<PokemonModel>, t: Throwable) {
                errorMessage.postValue(t.message)
            }

        })
    }
}