package com.example.proyectocursoandroid.screens.pokedex.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proyectocursoandroid.databinding.PokedexItemLayoutBinding
import com.example.proyectocursoandroid.models.pokedex.PokemonListItem
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient

class PokemonListAdapter(var context: Context, var listener: (PokemonListItem) -> Unit) : RecyclerView.Adapter<PokemonListAdapter.ViewHolder>() {

    var items = mutableListOf<PokemonListItem>()

    inner class ViewHolder(val binding: PokedexItemLayoutBinding) :RecyclerView.ViewHolder(binding.root)

    fun setPokemonList(list: List<PokemonListItem>){
        this.items = list.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = PokedexItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            with(items[position]){
                binding.pokemonName.text = this.name
                itemView.setOnClickListener {
                    listener(this)
                }
            }
        }
    }

    override fun getItemCount() = items.size
}