package com.example.proyectocursoandroid.screens.chat

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.proyectocursoandroid.models.chat.Message
import com.example.proyectocursoandroid.repositories.ChatRepository

class ChatViewModel(application: Application): AndroidViewModel(application) {

    private var repository: ChatRepository = ChatRepository(application)

    fun getMessages() = repository.getMessages()

    fun insertMessage(message: Message){
        repository.insertMessage(message)
    }
}