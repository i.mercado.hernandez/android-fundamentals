package com.example.proyectocursoandroid.screens.chat

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.proyectocursoandroid.databinding.ActivityChatBinding
import com.example.proyectocursoandroid.factories.ChatViewModelFactory
import com.example.proyectocursoandroid.models.chat.Message
import com.example.proyectocursoandroid.screens.chat.adapter.ChatItemAdapter

class ChatActivity: AppCompatActivity() {

    companion object {

        const val DEFAULT_MSG_LENGTH_LIMIT = 50

        fun start(context: Context){
            val intent = Intent(context, ChatActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityChatBinding
    private lateinit var adapter: ChatItemAdapter
    private val viewModel : ChatViewModel by viewModels {
        ChatViewModelFactory(this.application)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setup()
    }

    private fun setup(){
        adapter = ChatItemAdapter(this)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        binding.chatList.layoutManager = layoutManager
        binding.chatList.adapter = adapter
        viewModel.getMessages()?.observe(this, {
            showMessages(it)
        })
        binding.messageEditText.doOnTextChanged { text, _, _, _ ->
            binding.sendButton.isEnabled = text.toString().trim().isNotEmpty()
        }
        binding.messageEditText.filters = arrayOf(InputFilter.LengthFilter(DEFAULT_MSG_LENGTH_LIMIT))
        binding.sendButton.setOnClickListener {
            val message = Message(0, binding.messageEditText.text.toString().trim())
            viewModel.insertMessage(message)
            binding.messageEditText.text.clear()
        }
    }

    private fun showMessages(list: List<Message>) {
        adapter.setMessages(list)
    }
}