package com.example.proyectocursoandroid.screens.pokedex.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.proyectocursoandroid.R
import com.example.proyectocursoandroid.databinding.FragmentPokemonListBinding
import com.example.proyectocursoandroid.factories.PokedexViewModelFactory
import com.example.proyectocursoandroid.models.pokedex.PokemonListItem
import com.example.proyectocursoandroid.repositories.PokedexRepository
import com.example.proyectocursoandroid.screens.pokedex.PokedexViewModel
import com.example.proyectocursoandroid.screens.pokedex.adapter.PokemonListAdapter
import com.example.proyectocursoandroid.services.PokedexService

class PokemonListFragment: Fragment() {

    private lateinit var binding: FragmentPokemonListBinding
    private val viewModel: PokedexViewModel by activityViewModels{
        PokedexViewModelFactory(PokedexRepository(PokedexService.getInstance()))
    }
    lateinit var adapter: PokemonListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPokemonListBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup(){
        adapter = PokemonListAdapter(requireActivity()) { pokemon ->
            viewModel.setSelectedPokemon(pokemon)
        }
        viewModel.selectedPokemon.observe(viewLifecycleOwner, {
            goToPokemonDetailView(it)
        })
        binding.pokemonList.layoutManager = LinearLayoutManager(requireActivity())
        binding.pokemonList.adapter = adapter
        viewModel.pokemonList.observe(viewLifecycleOwner, {
            adapter.setPokemonList(it)
        })
        viewModel.errorMessage.observe(viewLifecycleOwner, {
            Toast.makeText(requireContext(), "Falló al Obtener Los pokemon", Toast.LENGTH_LONG).show()
        })
        viewModel.getPokemonList()
    }

    private fun goToPokemonDetailView(pokemon: PokemonListItem?){
        if(pokemon != null){
            if(findNavController().currentDestination?.id == R.id.pokemonListFragment){
                findNavController().navigate(R.id.action_pokemonListFragment_to_pokemonDetailFragment)
            }
        }
    }
}