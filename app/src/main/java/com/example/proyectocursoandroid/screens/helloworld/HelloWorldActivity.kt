package com.example.proyectocursoandroid.screens.helloworld

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.proyectocursoandroid.R
import com.example.proyectocursoandroid.databinding.ActivityHelloWorldBinding


class HelloWorldActivity : AppCompatActivity() {


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, HelloWorldActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityHelloWorldBinding
    private val viewModel: HelloWorldViewModel by viewModels()

    /*private val countDownTimer =  object : CountDownTimer(3000, 1000){
        override fun onTick(millisUntilFinished: Long) {}
        override fun onFinish() {
            binding.helloWorldInstructions.text = getString(R.string.hello_word_instructions_text)
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHelloWorldBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setup()
    }

    private fun setup(){
        binding.helloWorldNameInput.doOnTextChanged { text, _, _, _ ->
            viewModel.setCurrentName(text.toString())
        }
        viewModel.currentName.observe(this, { name ->
            handleName(name)
        })
    }

    private fun handleName(name: String){
        Log.d(HelloWorldActivity::class.java.simpleName, "handleName value = $name")
        if(name.isNotEmpty() && name.length >= 2){
            val nameToDisplay = "Hello world! From: $name"
            binding.helloWorldNameDisplay.visibility = View.VISIBLE
            binding.helloWorldInstructions.visibility = View.GONE
            binding.helloWorldNameDisplay.text = nameToDisplay
        } else {
            binding.helloWorldNameDisplay.visibility = View.GONE
            binding.helloWorldInstructions.visibility = View.VISIBLE
        }

        /*if( binding.helloWorldNameInput.text.toString().isNotEmpty()){
            val nameToDisplay = "Hello world! From: ${binding.helloWorldNameInput.text}"
            binding.helloWorldInstructions.visibility = View.GONE
            binding.helloWorldNameInput.visibility = View.GONE
            binding.submitButton.visibility = View.GONE
            binding.helloWorldNameDisplay.visibility = View.VISIBLE
            binding.tryAgainButton.visibility = View.VISIBLE
            binding.helloWorldNameDisplay.text = nameToDisplay
        } else {
            binding.helloWorldInstructions.text = getString(R.string.no_name_error)
            countDownTimer.start()
        }*/
    }

    private fun reset(){
        binding.helloWorldInstructions.visibility = View.VISIBLE
        binding.helloWorldNameInput.visibility = View.VISIBLE
        binding.submitButton.visibility = View.VISIBLE
        binding.helloWorldNameDisplay.visibility = View.GONE
        binding.tryAgainButton.visibility = View.GONE
        binding.helloWorldNameInput.setText("")
    }
}