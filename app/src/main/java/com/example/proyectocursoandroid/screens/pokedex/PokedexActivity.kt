package com.example.proyectocursoandroid.screens.pokedex

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.proyectocursoandroid.R
import com.example.proyectocursoandroid.databinding.ActivityPokedexBinding
import com.example.proyectocursoandroid.factories.PokedexViewModelFactory
import com.example.proyectocursoandroid.models.pokedex.PokemonListItem
import com.example.proyectocursoandroid.repositories.PokedexRepository
import com.example.proyectocursoandroid.screens.pokedex.adapter.PokemonListAdapter
import com.example.proyectocursoandroid.screens.pokedex.fragments.PokemonDetailFragment
import com.example.proyectocursoandroid.services.PokedexService

class PokedexActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context){
            val intent = Intent(context, PokedexActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityPokedexBinding
    private val viewModel: PokedexViewModel by viewModels{
        PokedexViewModelFactory(PokedexRepository(PokedexService.getInstance()))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPokedexBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onBackPressed() {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        when (navHostFragment.childFragmentManager.findFragmentById(R.id.nav_host_fragment)){
            is PokemonDetailFragment -> {
                viewModel.setSelectedPokemon(null)
                navHostFragment.findNavController().navigate(R.id.action_pokemonDetailFragment_to_pokemonListFragment)
            }
            else -> finish()
        }
    }

}