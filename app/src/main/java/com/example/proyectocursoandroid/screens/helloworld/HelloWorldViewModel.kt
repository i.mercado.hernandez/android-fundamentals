package com.example.proyectocursoandroid.screens.helloworld

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HelloWorldViewModel: ViewModel() {

    val currentName: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun setCurrentName(name: String){
        //Log.d(HelloWorldViewModel::class.java.simpleName, "setCurrentName value = $name")
        currentName.postValue(name)
    }
}