package com.example.proyectocursoandroid.repositories

import com.example.proyectocursoandroid.models.pokedex.PokemonModel
import com.example.proyectocursoandroid.models.pokedex.PokemonSpeciesResponse
import com.example.proyectocursoandroid.services.PokedexService
import retrofit2.Call

class PokedexRepository constructor(private val pokedexService: PokedexService) {

    fun getPokemonList(): Call<PokemonSpeciesResponse> = pokedexService.getPokemonList()

    fun getPokemonDetail(name: String): Call<PokemonModel> = pokedexService.getPokemonDetail(name)
}