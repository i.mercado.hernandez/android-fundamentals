package com.example.proyectocursoandroid.repositories

import android.app.Application
import com.example.proyectocursoandroid.models.chat.ChatDatabase
import com.example.proyectocursoandroid.models.chat.Message
import com.example.proyectocursoandroid.models.chat.MessageDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class ChatRepository(application: Application): CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var messageDao: MessageDao?

    init {
        val database = ChatDatabase.getInstance(application)
        messageDao = database.messageDao()
    }

    fun getMessages() = messageDao?.getMessages()

    fun insertMessage(message: Message){
        launch { saveMessageIntoDatabase(message) }
    }

    private suspend fun saveMessageIntoDatabase(message: Message){
        withContext(Dispatchers.IO){
            messageDao?.insertMessage(message)
        }
    }
}