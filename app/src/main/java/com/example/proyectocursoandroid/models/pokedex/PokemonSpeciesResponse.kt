package com.example.proyectocursoandroid.models.pokedex

data class PokemonSpeciesResponse(
    val results: List<PokemonListItem>
)
