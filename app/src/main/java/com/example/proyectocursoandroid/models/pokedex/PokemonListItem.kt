package com.example.proyectocursoandroid.models.pokedex

data class PokemonListItem(
    val name: String
)
