package com.example.proyectocursoandroid.models.pokedex

import com.google.gson.annotations.SerializedName

data class PokemonModel(
    val name: String,
    val id: Int,
    val abilities: List<PokemonAbilities>,
    val moves: List<PokemonMoves>
)

data class PokemonAbilities(
    val ability: Ability,
    @SerializedName("is_hidden") val hidden: Boolean
)

data class Ability(
    val name: String
)

data class PokemonMoves(
    @SerializedName("move")val move: Move
)

data class Move(
    val name: String
)

