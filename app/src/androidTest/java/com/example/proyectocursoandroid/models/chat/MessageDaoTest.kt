package com.example.proyectocursoandroid.models.chat

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class MessageDaoTest {

    private lateinit var chatDatabase: ChatDatabase
    private lateinit var messageDao: MessageDao

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun initDatabase(){
        chatDatabase = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ChatDatabase::class.java)
            .build()
        messageDao = chatDatabase.messageDao()
    }

    @After
    fun closeDatabase(){
        chatDatabase.close()
    }

    @Test
    fun insertMessageTest(){
        val message = Message(0, "Este es un mensaje de prueba")
        messageDao.insertMessage(message)
        val currentMessages = messageDao.getMessages().blockingObserve()
        assert(currentMessages?.firstOrNull { it.message == message.message } != null)
    }

    private fun <T> LiveData<T>.blockingObserve(): T? {
        var value : T? = null
        val latch = CountDownLatch(1)

        val observer = Observer<T> { t ->
            value = t
            latch.countDown()
        }

        observeForever(observer)

        latch.await(2, TimeUnit.SECONDS)
        return value
    }
}