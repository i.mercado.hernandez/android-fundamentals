package com.example.proyectocursoandroid.screens.chat

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.proyectocursoandroid.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ChatActivityTest {

    @get:Rule
    var activityScenarioRule = ActivityScenarioRule(ChatActivity::class.java)

    @Test
    fun checkSendButtonEnabledTest(){
        val messageTyped = "Mensaje de prueba"
        onView(withId(R.id.message_edit_text)).perform(replaceText(messageTyped))
        onView(withId(R.id.send_button)).check { view, _ ->
            view.isEnabled
        }
    }
}