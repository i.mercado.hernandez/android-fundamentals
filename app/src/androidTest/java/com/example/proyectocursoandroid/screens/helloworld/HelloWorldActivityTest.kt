package com.example.proyectocursoandroid.screens.helloworld

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.proyectocursoandroid.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HelloWorldActivityTest {


    @get:Rule
    var activityRule = ActivityScenarioRule(HelloWorldActivity::class.java)

    @Test
    fun helloWorldDisplayTextCondition(){
        val nameTyped = "Ahmed Mercado"
        onView(withId(R.id.hello_world_name_input)).perform(replaceText(nameTyped))
        onView(withId(R.id.hello_world_name_display)).check(matches(isDisplayed()))
    }
}